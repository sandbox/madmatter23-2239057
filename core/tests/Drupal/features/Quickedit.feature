Feature: Quick edit Link
  Quick edit link displays quick edit link to edit content.
  
  @api @javascript
  Scenario: Quick Edit Link appears in the content when Quick edit link is clicked.
    Given I am logged in as a user with the "administrator" role
    And I am viewing an "article" node with the title "Aardvark"
    When I click the element with css selector "button.toolbar-icon-edit"
    And I should see "Open Add new comment configuration options" in the "button.trigger" element
    And I click the element with CSS selector "button.trigger"
    And I should see the link "Quick edit"
    And I click "Quick edit"
    Then I should see an "div.quickedit-editable" element
