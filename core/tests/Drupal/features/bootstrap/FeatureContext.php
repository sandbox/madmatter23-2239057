<?php

use Drupal\DrupalExtension\Context\DrupalContext,
  Drupal\DrupalExtension\Event\EntityEvent;

use Behat\Behat\Exception\PendingException;

use Behat\Gherkin\Node\PyStringNode,
  Behat\Gherkin\Node\TableNode;

class FeatureContext extends DrupalContext {
  /**
   *
   * @When /^(?:|I )click the element with CSS selector "([^"]*)"$/
   * @When /^(?:|I )click the element with css selector "([^"]*)"$/
   */
  public function iClickTheElementWithCssSelector($css_selector) {
    $element = $this->getSession()->getPage()->find("css", $css_selector);
    if (empty($element)) {
      throw new \Exception(sprintf("The page '%s' does not contain the css selector '%s'", $this->getSession()->getCurrentUrl(), $css_selector));
    }
    $element->click();
  }
}
