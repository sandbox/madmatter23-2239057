Feature: Toolbar
  The toolbar provides site administration operations.

  @api @javascript
  Scenario: Toolbar Manage menus appear when the Manage tab is clicked
    Given I am logged in as a user with the "administrator" role
    When I click "Manage"
    Then I should see "Content"
    # Clean up.
    And I click "Manage"

  @api @javascript
  Scenario: Toolbar Manage submenus appear when the Content menu item twisty is clicked
    Given I am logged in as a user with the "administrator" role
    When I click "Manage"
    And I click the element with CSS selector ".toolbar-icon.toolbar-icon-toggle-vertical"
    And I click the element with CSS selector ".toolbar-handle"
    Then I should see "Comments"
    # Clean up.
    And I click "Manage"
